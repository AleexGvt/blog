import React from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";

import Art1 from "../images/logo.png";

import "./css/ArticuloCard.css";

export default function ArticuloCard() {
  return (
    <Card className="hvr-grow-shadow card-height">
      <CardActionArea>
        <CardMedia
          component="img"
          alt="Contemplative Reptile"
          height="200"
          image={Art1}
          title="Contemplative Reptile"
          className="roundImg"
        />
        <CardContent style={{backgroundColor:"gray"}}>
          <div className="text-center">
            <h4>TITULO DEL ARTICULO</h4>
            Lorem ipsum dolor sit amet consectetur adipiscing elit facilisis,
            neque nascetur curae feugiat lobortis ultricies morbi nec malesuada,
            nunc commodo odio turpis porttitor ad nulla. Dui est sodales potenti
            tempor consequat himenaeos phasellus diam aliquam molestie, praesent
            vivamus sapien luctus hac nam vel iaculis commodo, nunc dis rutrum
            quisque per justo enim erat tempus.
          </div>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
