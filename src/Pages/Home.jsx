import React from "react";

import styles from "./css/Home.module.css";
import Logo from "../images/logo.png";
import Articulo from "../components/ArticuloCard";

export default function Home() {
  return (
    <div>
      <div className={styles.portadaHome}>  
      <div className={styles.verticalcenter}>
      <div className="container"> 
        <div className="row"> 
          <div className="col-sm">
          <Articulo />  
          </div>
          <div className="col-sm">
          <Articulo />  
          </div>
          <div className="col-sm">
          <Articulo />  
          </div>
        </div>
      </div>
      </div>
     </div>
    </div>
  );
}