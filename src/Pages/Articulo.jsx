import React from "react";
import { useParams } from "react-router-dom";

import Logo from "../images/logo.png";
import Card1 from "../images/fondoR.jpg";
import styles from "./css/Articulo.module.css";
import Articulos from "../Articulo/Articulos.jsx";
import Comentario from "../Articulo/Comentario.jsx"

export default function Home() {
    const { id } = useParams();
    return (
    <div>
      <div className={styles.portadaHome}>  
      <div className={styles.verticalcenter}>
      <div>
      <Articulos />
    </div>
      <div>
      </div>
      </div>
     </div>
    </div>
  
  );
}