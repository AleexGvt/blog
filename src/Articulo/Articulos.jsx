import React from "react";
import Logo from "../images/logo.png";
import Card1 from "../images/fondoR.jpg";
import styles from "../Pages/css/Articulo.module.css";
import Comentarios from "../Articulo/Comentario";

export default function Articulos() {
  return (
    <div className="container py-5">
      <h3 className="text-center pb-3">Titulo del articulo</h3>
      <div className={`row ${styles.customRow}`}>
        <div className={`col-md-3 ${styles.img__container}`}>
          <img src={Card1} alt="" className={`${styles.imagenUno}`} />
        </div>
        <div className={`col-md ${styles.textCenter}`}>
          Lorem ipsum dolor sit amet consectetur adipiscing elit facilisis,
          neque nascetur curae feugiat lobortis ultricies morbi nec malesuada,
          nunc commodo odio turpis porttitor ad nulla. Dui est sodales potenti
          tempor consequat himenaeos phasellus diam aliquam molestie, praesent
          vivamus sapien luctus hac nam vel iaculis commodo, nunc dis rutrum
          quisque per justo enim erat tempus. Ut enim netus class placerat
          praesent iaculis sociis faucibus, taciti tortor purus etiam feugiat
          venenatis habitasse tincidunt a, nec volutpat ultrices mus dictum
          ultricies aliquet. Lorem ipsum dolor sit amet consectetur adipiscing
          elit facilisis, neque nascetur curae feugiat lobortis ultricies morbi
          nec malesuada, nunc commodo odio turpis porttitor ad nulla. Dui est
          sodales potenti tempor consequat himenaeos phasellus diam aliquam
          molestie, praesent vivamus sapien luctus hac nam vel iaculis commodo,
          nunc dis rutrum quisque per justo enim erat tempus. Ut enim netus
          class placerat praesent iaculis sociis faucibus, taciti tortor purus
          etiam feugiat venenatis habitasse tincidunt a, nec volutpat ultrices
          mus dictum ultricies aliquet. Lorem ipsum dolor sit amet consectetur
          adipiscing elit facilisis, neque nascetur curae feugiat lobortis
          ultricies morbi nec malesuada, nunc commodo odio turpis porttitor ad
          nulla. Dui est sodales potenti tempor consequat himenaeos phasellus
          diam aliquam molestie, praesent vivamus sapien luctus hac nam vel
          iaculis commodo, nunc dis rutrum quisque per justo enim erat tempus.
          Ut enim netus class placerat praesent iaculis sociis faucibus, taciti
          tortor purus etiam feugiat venenatis habitasse tincidunt a, nec
          volutpat ultrices mus dictum ultricies aliquet.
        </div>
      </div>
      <br></br>
      <Comentarios />
    </div>
  );
}
