import logo from './logo.svg';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import './App.css';
import Home from "./Pages/Home.jsx"
import Login from "./Pages/Login.jsx"
import Registro from "./Pages/Registro.jsx"
import Layout from "./components/Layout";
import Articulo from "./Pages/Articulo.jsx";

function App() {
  return (
    <BrowserRouter>
      
      <Switch>
        <Route exact path="/Home">
          <Layout>
            <Home />
            </Layout>  
        </Route>
        <Route exact path="/Login">
            <Login />
        </Route>
        <Route exact path="/Registro">
            <Registro />
        </Route>
        <Route exact path="/Articulo">
        <Layout>
        <Articulo />
        </Layout>  
      </Route>
          <Route exact path="/Articulo/:id">
            <Layout>
            <Articulo />
            </Layout>
          </Route>
      </Switch>
          
    </BrowserRouter>
  );
}

export default App;
